import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export interface StoreState {
  message: string
};

export default new Vuex.Store<StoreState>({
  state: {
    message: 'Hello World'
  }
});