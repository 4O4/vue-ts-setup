import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '../components/Main.vue';
import SecondPage from '../components/SecondPage.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Main
  },
  {
    path: '/another',
    component: SecondPage,
  }
];

export default new VueRouter({
  routes
});